<?php

/**
 * String Helper Class
 *
 * @category  String Helper
 * @package   PHP
 * @author    Scott Clark <scott@developerla.com>
 * @copyright Copyright (c) 2014
 * @license   http://opensource.org/licenses/gpl-3.0.html
 * @version   1.3
 **/

class StringHelper
{ 	
	// Used for encryption, it can contain alphanumerics and symbols.
	const SALT= 'A phrase that is unique to you!';
	
	/** 
	 * Replaces Windows line endings with Unix line endings
	 *
	 * @param	string	$str	The string you wish to normalize
	 * @return	string 
	 */
	public function normalizeEol($str)
	{
		$str = str_replace( "\r\n", "\n", $str );
		$str = str_replace( "\r", "\n", $str );

		return $str;
	}
	
	/** 
	 * Returns a normalized phone number (United States)
	 *
	 * @param	string	$phone			The unformatted phone number
	 * @param	bool	$use_area_code	Does this number contain the area code
	 * @param	string	$seperator		The character you want to seperate the number with
	 * @return	string
	 */
	public function normalizePhone($phone,$use_area_code=true,$separator='-')
	{
		$valid_number = false;
		$message = '';
		
		//convert letters to numbers
		$phone = $this->convertLettersFromPhone($phone);
		
		//strip all non-numeric characters
		$digits = preg_replace('/\D/i', '', $phone);

		// strips country code, if present
		if(strpos($digits,'1') === 0)
			$digits = substr($digits,1,strlen($digits));

		if($use_area_code){
			if(strlen($digits) == 10){
				$valid_number	= true;
				$result			= preg_replace('/([\d]{3})([\d]{3})([\d]{4})/i', '$1'.$separator.'$2'.$separator.'$3', $digits);
			}else{
				$result			= $phone.' is not a valid phone number';
			}
		}else{
			if(strlen($digits) == 7){
				$valid_number = true;
				$result			= preg_replace('/([\d]{3})([\d]{4})/i', '$1'.$separator.'$2', $digits);
			}else{
				$result			= $phone.' is not a valid phone number';
			}
		}

		return $result;
	}
	
	function convertLettersFromPhone($phone_number)
	{
		$num				= $this->removeNonAlphanumerics(strtolower($phone_number));
		$results			= '';
		$letters_to_nums	= array(
				'a'=>2,
				'b'=>2,
				'c'=>2,
				'd'=>3,
				'e'=>3,
				'f'=>3,
				'g'=>4,
				'h'=>4,
				'i'=>4,
				'j'=>5,
				'k'=>5,
				'l'=>5,
				'm'=>6,
				'n'=>6,
				'o'=>6,
				'p'=>7,
				'r'=>7,
				's'=>7,
				't'=>8,
				'u'=>8,
				'v'=>8,
				'w'=>9,
				'x'=>9,
				'y'=>9,
		);
		
		$chars = str_split($num);
		foreach($chars as $n)
		{
			if(isset($letters_to_nums[$n]))
				$results .= $letters_to_nums[$n];
			else
				$results .= $n;
		}
		
		return $results;
	}
	
	/** 
	 * Validates that an email is properly formed
	 *
	 * @param	string	$email			The email address to validate
	 * @return	bool
	 */
	public function validateEmail($email)
	{
		$valid_email = false;
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email))
		{
			$valid_email = true;
		}
		
		return $valid_email;
	}

	/** 
	 * Prepends 'http' or 'https' if the protocol is not present
	 *
	 * @param	string	$url	The URL to normalize
	 * @param	bool	$secure	Should the return contain https
	 * @return	string
	 */
	public function normalizeUrl($url,$secure=false,$prepend_www=false)
	{
		$protocol = ($secure) ? 'https://' : 'http://';
		
		//verify if it already has http or https
		preg_match('/http[s]*/i', $url, $prt);
		preg_match('/https+/i', $url, $ssl);
		preg_match('/([w]{3})/i', $url, $www);
		$has_protocol	= (count($prt) > 0) ? 1 : 0;
		$has_ssl		= (count($ssl) > 0) ? 1 : 0;
		$has_www		= (count($www) > 0) ? 1 : 0;
		
		//add www
		if(!$has_protocol && $prepend_www)
		{
			if(!$has_www && $prepend_www)
				$url = 'www.'.$url;
		}else{
			if(!$has_www && $prepend_www)
				$url = preg_replace('/(http[s]*:\/\/)/i', '$1www.', $url);
		}
		
		// add http
		if($secure && $has_protocol){
			if(!$has_ssl)
				$url = preg_replace('/http:/i', 'https:', $url);
		}else if(!$secure && $has_ssl){
			$url = preg_replace('/https:/i', 'http:', $url);
		}
		
		if($has_protocol){
			$formatted_url = trim($url);
		}else{
			$formatted_url = $protocol.trim($url);
		}
	
		return $formatted_url;
	}
	
	/** 	
	 * Removes spaces (single or multiple) and replaces with a desired character.
	 *
	 * @param	string		$string			The string to be altered
	 * @param	char		$replacement	The character that will replace spaces
	 * @return	string
	 */
	public function removeSpaces($string,$replacement='_')
	{
		$str = preg_replace('/([ ])+/i',$replacement,$string);
		return $str;
	}

	/** 	
	 * Removes any character that is not alphanumeric or a space
	 *
	 * @param	string		$str			The string to be altered
	 * @return	string
	 */	
	public function removeNonAlphanumerics($str)
	{ 
		$str_len	= strlen($str); 
		$result		= "";
	 
		for( $i = 0 ; $i < $str_len ; $i++ )
		{ 
			// 48 - 57 = 0-9, 65 - 90 = A-Z, 97 - 122 = a-z, 32 = space
			if(ord($str[$i]) >= 48 && ord($str[$i]) <= 57 || ord($str[$i]) >= 65 && ord($str[$i]) <= 90 || ord($str[$i]) >= 97 && ord($str[$i]) <= 122 || ord($str[$i]) == 32)
			{ 
				$result .= $str[$i];
			}
		}
		return trim($result);
	}

	/** 	
	 * Replaces accented characters with regular characters, removes/replaces spaces
	 * and removes non-alphanumeric characters
	 *
	 * @param	string		$str			The filename to be normalized
	 * @param	char		$replace		The character that will replace spaces
	 * @return	string
	 */		
	public function normalizeFilename($str,$replace='_')
	{    
		$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 
									'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 
									'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 
									'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 
									'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 
									'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 
									'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 
									'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 
									'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 
									'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 
									'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 
									'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 
									'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' 
									);
		$str = strtr( $str, $unwanted_array );
		$str = $this->removeNonAlphanumerics($str);
		$str = $this->removeSpaces($str,$replace);

		return $str;
	}
	
	/** 
	 * Converts a string into its phonetic equivalent
	 *
	 * @param	string	$str	The string to be converted
	 * @param	string	$type	The type of conversion: military or civilian
	 * @return	array
	 */	
	function convertToPhoneticAlphabet($str,$type='military')
	{
		$string = strtolower($str);
		$phonetics = array(
						'military'=>array(
						'a'=>'Alpha',
						'b'=>'Bravo',
						'c'=>'Charlie',
						'd'=>'Delta',
						'e'=>'Echo',
						'f'=>'Foxtrot',
						'g'=>'Golf',
						'h'=>'Hotel',
						'i'=>'India',
						'j'=>'Juliet',
						'k'=>'Kilo',
						'l'=>'Lima',
						'm'=>'Mike',
						'n'=>'November',
						'o'=>'Oscar',
						'p'=>'Papa',
						'q'=>'Quebec',
						'r'=>'Romeo',
						's'=>'Sierra',
						't'=>'Tango',
						'u'=>'Uniform',
						'v'=>'Victor',
						'w'=>'Whiskey',
						'x'=>'X-ray',
						'y'=>'Yankee',
						'z'=>'Zulu'
						),
						'civilian'=> array(
						'a'=>'Adam',
						'b'=>'Boy',
						'c'=>'Charles',
						'd'=>'David',
						'e'=>'Edward',
						'f'=>'Frank',
						'g'=>'George',
						'h'=>'Henry',
						'i'=>'Ida',
						'j'=>'John',
						'k'=>'King',
						'l'=>'Lincoln',
						'm'=>'Mary',
						'n'=>'Nora',
						'o'=>'Ocean',
						'p'=>'Paul',
						'q'=>'Queen',
						'r'=>'Robert',
						's'=>'Sam',
						't'=>'Tom',
						'u'=>'Union',
						'v'=>'Victor',
						'w'=>'William',
						'x'=>'Xray',
						'y'=>'Yellow',
						'z'=>'Zebra'
						)
					);

		$results = '';
		$str_arr = str_split($string);
		foreach($str_arr as $s)
		{
			if(isset($phonetics[$type][$s]))
				$results[] = $phonetics[$type][$s];
			else
				$results[] = $s;
		}
		return $results;
	}
	
	/** 
	 * Encrypts a string using your SALT in the Base64 format
	 *
	 * @param	string	$text	The string to be encrypted
	 * @return	string
	 */	
	public function b64_encrypt($text) 
	{ 
	    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
	} 

	/** 
	 * Decrypts a string using your SALT in the Base64 format
	 *
	 * @param	string	$$text	The string to be decrypted
	 * @return	string
	 */		
	public function b64_decrypt($text) 
	{ 
	    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
	}
	
	/** 	
	 * Prints a human readable string or array - used for debugging
	 *
	 * @param	array	$content	The contents of the string or array you wish to view
	 * @return	string
	 */
	public function printContent($content)
	{
		echo "<pre>";
		print_r($content);
		echo "</pre>";
	}
}

?>