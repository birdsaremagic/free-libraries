<?
// INCLUDE FILES
include('includes/stringhelper.php');

// VARIABLES
$results	= '';

// PROCESS CONVERSION
if(isset($_POST['url']) && strlen(trim($_POST['url'])) > 0)
{
	$url		= new StringHelper();
	$normalized	= $url->normalizeUrl($_POST['url'],$_POST['ssl'],$_POST['www']);
	$results	= "<div>".$normalized."</div>";	
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Normalize URL</title>
		<style>
		form.fatform input[type=text]{
			display:block;
			height: 30px;
			width: 300px;
			padding: 5px 10px 5px 10px;
			color: #666;
			font-size: 16px;
			font-family: sans-serif;
			margin-bottom: 10px;
		}
		form.fatform input[type=submit]{
			margin: 6px 0 0 0;
			width: 75px;
		}
		#results{
			margin: 10px 0 10px 10px;
		}
		#results div{
			margin: 4px;
			font-family: serif;
			font-size: 18px;
			color: #888;
		}
		span.bold{
			font-family: serif;
			font-size: 18px;
			color: #000;
			font-weight:bold
		}
		</style>
	</head>
	
	<body>
		<form name="url" id="normalize_url" action="<?=$_SERVER['PHP_SELF']?>" method="POST" class="fatform">
			<input type="text" name="url">
			<input type=checkbox name="ssl" value='1'>Secure? &nbsp;&nbsp;
			<input type=checkbox name="www" value='1'>Prepend www?<br />
			<input type="submit" value="GO">
		</form>
		<div id="results"><?=$results?></div>
	</body>
</html>